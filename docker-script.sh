{
  docker run -i --name nikitos -v `pwd`:/usr/src/app -w /usr/src/app node:alpine sh -c "npm install husky@2.4 && npm run lint && npm test" &&
  LC_TIME=en_US date > index.html &&
  echo '<pre>' >> index.html &&
  docker run --rm grycap/cowsay /usr/games/cowsay " Nikita's tests were passed!" >> index.html &&
  echo '</pre>' >> index.html &&
  docker rm nikitos 
} || { 
  echo '<pre>' > index.html &&
  docker logs nikitos 2>&1 | tee -a index.html &&
  echo '</pre>' >> index.html
  docker rm nikitos 
}
echo 'FROM nginx' > Dockerfile
echo 'COPY index.html /usr/share/nginx/html' >> Dockerfile
docker login registry.gitlab.com -u "Mykyta Kravchenko" -p $ACCESS_TOKEN
docker pull registry.gitlab.com/mykyta.kravchenko/my_array_test_cases/logs
docker tag registry.gitlab.com/mykyta.kravchenko/my_array_test_cases/logs registry.gitlab.com/mykyta.kravchenko/my_array_test_cases/logs:previous
docker push registry.gitlab.com/mykyta.kravchenko/my_array_test_cases/logs:previous
docker rmi registry.gitlab.com/mykyta.kravchenko/my_array_test_cases/logs:previous
docker build -t registry.gitlab.com/mykyta.kravchenko/my_array_test_cases/logs .
docker push registry.gitlab.com/mykyta.kravchenko/my_array_test_cases/logs
ssh nikita.kravchenko@104.248.138.53 ./docker.ch