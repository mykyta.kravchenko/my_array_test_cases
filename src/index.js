function MyArray(arrLength) {
  if (!new.target) {
    return new MyArray(...arguments);
  }

  Object.defineProperty(this, '_length', { value: arguments.length, writable: true, enumerable: false });
  Object.defineProperty(this, 'length', {
    get() {
      return this._length;
    },
    set(len) {
      if (Number.isInteger(len) && len >= 0) {
        if (len < this._length) {
          for (let i = this._length; i >= len; i--) {
            delete this[i];
          }
        }

        this._length = len;
      } else {
        throw new RangeError('Invalid array length');
      }
    }
  });

  if (!arguments.length) {
    this.length = 0;
    return this;
  }

  if (arguments.length === 1) {
    if (arrLength < 0) {
      throw new RangeError('Invalid array length');
    }

    if (arrLength >= 2 ** 32) {
      throw new RangeError('Invalid array length');
    }

    if (typeof arrLength === 'number' && isNaN(arrLength)) {
      throw new RangeError('Invalid array length');
    }

    if (Number.isInteger(arrLength)) {
      this.length = arrLength;
      return this;
    }
  }

  for (let i = 0; i < arguments.length; i++) {
    this[i] = arguments[i];
  }

  const proxy = new Proxy(this, {
    get(target, key) {
      return target[key];
    },
    set(target, key, value) {
      const convertedKey = Number(key);

      if (!isNaN(convertedKey) && convertedKey >= target.length) {
        target.length = convertedKey + 1;
        target[convertedKey] = value;
      } else {
        target[key] = value;
      }
    }
  });

  return proxy;
}

MyArray.prototype[Symbol.iterator] = function() {
  let current = 0;
  const last = this.length;

  return {
    next: () => {
      if (current < last) {
        const valueNumber = current;
        current += 1;
        return {
          done: false,
          value: this[valueNumber]
        };
      }

      return { done: true };
    }
  };
};

MyArray.prototype.push = function(item, ...items) {
  if (!this.length || !Number.isInteger(this.length)) {
    this.length = 0;
  }

  if (!arguments.length) {
    return this.length;
  }

  let newLength = this.length;
  this[newLength] = item;
  newLength += 1;

  if (!items.length) {
    this.length = newLength;
    return this.length;
  }

  for (let i = 1; i <= items.length; i++) {
    this[newLength] = items[i - 1];
    newLength += 1;
  }
  this.length = newLength;
  return this.length;
};

MyArray.prototype.pop = function() {
  if (!this.length) {
    this.length = 0;
  }

  if (this.length === 0) {
    return undefined;
  }

  const item = this[this.length - 1];
  delete this[this.length - 1];
  this.length -= 1;
  return item;
};

MyArray.from = function(arrayLike, ...args) {
  const argLength = arrayLike.length;
  const arr = new MyArray();

  if (!argLength) {
    return arr;
  }

  for (let i = 0; i < argLength; i++) {
    if (args[0]) {
      const result = args[0].call(args[1], arrayLike[i], i, arrayLike);
      arr.push(result);
    } else {
      arr.push(arrayLike[i]);
    }
  }
  return arr;
};

MyArray.prototype.map = function(callback, ...args) {
  const currentLength = this.length;
  const arr = new MyArray(currentLength);

  for (let i = 0; i < currentLength; i++) {
    if (i in this) {
      if (args[0]) {
        const item = callback.call(args[0], this[i], i, this);
        arr[i] = item;
      } else {
        const item = callback(this[i], i, this);
        arr[i] = item;
      }
    }
  }

  return arr;
};

MyArray.prototype.forEach = function(callback, ...args) {
  const currentLength = this.length;

  for (let i = 0; i < currentLength; i++) {
    if (i in this) {
      if (args[0]) {
        callback.call(args[0], this[i], i, this);
      } else {
        callback(this[i], i, this);
      }
    }
  }
};

MyArray.prototype.reduce = function(callback, ...args) {
  const currentLength = this.length;
  let initValueCheck = true;

  if (arguments.length <= 1) {
    initValueCheck = false;
  }

  let initialValue = this[0];

  if (initValueCheck === false && !currentLength) {
    throw new TypeError('Reduce of empty array with no initial value');
  }

  if (initValueCheck) {
    initialValue = args[0];

    for (let i = 0; i < currentLength; i++) {
      if (i in this) {
        initialValue = callback(initialValue, this[i], i, this);
      }
    }
    return initialValue;
  }

  for (let i = 1; i < currentLength; i++) {
    if (i in this) {
      initialValue = callback(initialValue, this[i], i, this);
    }
  }
  return initialValue;
};

MyArray.prototype.filter = function(callback, ...args) {
  const arr = new MyArray();

  const cuurentLength = this.length;

  for (let i = 0; i < cuurentLength; i++) {
    const thisEl = this[i];

    if (i in this) {
      if (callback.call(args[0], thisEl, i, this)) {
        arr.push(thisEl);
      }
    }
  }
  return arr;
};

MyArray.prototype.toString = function() {
  let string = '';

  for (let i = 0; i < this.length; i++) {
    if (this[i]) {
      string += this[i];
    }

    if (i !== this.length - 1) {
      string += ',';
    }
  }
  return string;
};

MyArray.prototype.sort = function(callback) {
  const curLength = this.length;
  const thisArrClone = new MyArray(...this);
  const cbResultsArray = new MyArray();

  if (!callback) {
    for (let i = 0; i < curLength - 1; i++) {
      for (let j = i + 1; j < curLength; j++) {
        if (String(this[i]) > String(this[j])) {
          const temp = this[i];
          this[i] = this[j];
          this[j] = temp;
        }
      }
    }

    return this;
  }

  for (let i = 1; i < curLength; i++) {
    cbResultsArray.push(callback(this[i], this[i - 1]));
  }

  for (let i = 0; i < curLength; i++) {
    for (let j = i + 1; j < curLength; j++) {
      if (cbResultsArray[i] === -1) {
        const temp = this[i];
        this[i] = this[j];
        this[j] = temp;
      }
    }
  }

  return this;
};

MyArray.prototype.slice = function(start, end) {
  const curLength = this.length;
  let startIndex = 0;
  let endIndex = curLength;
  const arr = new MyArray();


  if (Number.isInteger(Number(start))) {
    startIndex = start < 0 ? curLength + start : start;
  }

  if (end && !Number.isInteger(Number(end))) {
    return arr;
  }

  if (Number.isInteger(Number(end))) {
    endIndex = end < 0 ? curLength + end : end;

    if (end > curLength) {
      endIndex = curLength;
    }
  }

  for (let i = startIndex; i < endIndex; i++) {
    arr.push(this[i]);
  }
  return arr;
};

MyArray.prototype.indexOf = function(searchElement, ...fromIndex) {
  let startIndex = 0;
  const currLength = this.length;

  if (arguments.length === 2) {
    if (fromIndex[0] >= this.length) {
      return -1;
    }
    startIndex = fromIndex[0];

    if (fromIndex[0] < 0) {
      startIndex = currLength - fromIndex[0];
    }
  }


  for (let i = startIndex; i < currLength; i++) {
    if (searchElement === this[i]) {
      return i;
    }
  }
  return -1;
};

MyArray.prototype.indexOf = function(searchElement, ...fromIndex) {
  let startIndex = 0;
  const currLength = this.length;

  if (arguments.length === 2) {
    if (fromIndex[0] >= this.length) {
      return -1;
    }
    startIndex = fromIndex[0];

    if (fromIndex[0] < 0) {
      startIndex = currLength - fromIndex[0];
    }
  }


  for (let i = startIndex; i < currLength; i++) {
    if (searchElement === this[i]) {
      return i;
    }
  }
  return -1;
};

MyArray.prototype.splice = function(start, deleteCount, ...args) {
  const deletedElements = new MyArray();
  const restArr = new MyArray();
  const curLength = this.length;
  let startIndex = 0;
  let endIndex = curLength;
  const startValue = Number(start);

  if (startValue) {
    startIndex = startValue < 0 ? Math.floor(curLength + startValue) : Math.floor(startValue);
    startIndex = startIndex < 0 ? 0 : startIndex;
  }

  if (Number.isInteger(deleteCount)) {
    endIndex = startIndex + deleteCount;

    if (deleteCount >= curLength - startIndex) {
      endIndex = curLength;
    }
  }

  if (Number.isNaN(deleteCount)) {
    endIndex = startIndex;
  }

  for (let i = endIndex; i < curLength; i++) {
    restArr.push(this[i]);
  }

  let newLength = curLength;

  for (let i = startIndex; i < endIndex; i++) {
    deletedElements.push(this[i]);
    newLength -= 1;
  }
  this.length = newLength;
  let curIndex = 0;

  if (args.length) {
    for (let i = startIndex; i < newLength + args.length; i++) {
      this[i] = args[curIndex];
      curIndex += 1;
    }
  }
  newLength += curIndex;
  this.length = newLength;
  curIndex = 0;

  startIndex += args.length;

  for (let i = startIndex; i < startIndex + restArr.length; i++) {
    this[i] = restArr[curIndex];
    curIndex += 1;
  }

  this.length = startIndex + curIndex;

  return deletedElements;
};

module.exports = MyArray;
